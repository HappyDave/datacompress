#!/usr/bin/python
# -*- coding: utf-8 -*-

import math
import time
import argparse
import os

"""
huffman.py - David Santos, Oscar Fernandez 2014
"""


class Node(object):
    """
    Classe Node: utilitzada per a crear l'arbre de Huffman
    """
    def __init__(self):
        self.data = None
        self.code = None
        self.freq = 1
        self.parent = None
        self.left = None
        self.right = None

    def __unicode__(self):
        data = self.data
        if self.data > 127:
            data = data.encode('hex')
        elif self.data == '\n':
            data = '\\n'

        return u"(" + data + u" : " + str(self.freq) + u')'

    def __str__(self):
        return unicode(self).encode('utf-8')


def calcula_stats(nodes, mida):
    """
    Funció que donat un iterable de nodes fulla de l'arbre de Huffman, retorna el rendiment, l'entropia i l'eficiència.
    """
    mida = float(mida)
    rendiment = float()
    entropia = float()
    dimension = 0  # Comptador de símbols del vocabulari

    for i in nodes:
        probabilitat = i.freq / mida
        longitud_codi = len(i.code)
        rendiment += longitud_codi * probabilitat
        entropia += probabilitat * math.log(1 / probabilitat, 2)
        dimension += 1

    eficiencia = entropia / (rendiment * math.log(dimension, 2))

    return rendiment, entropia, eficiencia


def establir_codis(node, prefix=str()):
    """
    Métode utilitzat per recorre l'arbre Huffman i establir la codificació dels símbols als nodes corresponents.
    """
    if not node.parent:
        codi = str()
    else:
        codi = str(node.code)

    codi_actual = prefix + codi
    if not node.left:  # Cas directe: estem a una fulla
        node.code = codi_actual
    else:  # Altre cas, cridem recursivament pels dos fills enviant el nou codi
        establir_codis(node.left, codi_actual)
        establir_codis(node.right, codi_actual)


def crea_arbre(nodes_in):
    """
    Funció que donat un iterable de nodes, retorna el node arrel de l'arbre construit segons Huffman
    """
    nodes = list(nodes_in)  # Fem una llista a partir de l'iterador
    while len(nodes) > 1:
        nodes.sort(key=lambda x: x.freq, reverse=True)  # Ordenem la llista de nodes de més freqüent a menys
        
        if verbose:
            for i in nodes:
                print i,
            print;print;

        n1 = nodes.pop()  # Retirem els 2 nodes amb menor frequencia (els dos ultims)
        n2 = nodes.pop()
        n1.code = 0  # Establim codi (menor=0, major=1)
        n2.code = 1

        n3 = Node()  # Pare

        if verbose:
            if n1.data == '\n':
                n3.data = '\\n' + n2.data
            elif n2.data == '\n':
                n3.data = n1.data + '\\n'
            else:
                n3.data = n1.data + n2.data

        n3.freq = n1.freq + n2.freq  # Sumem frequencies
        n3.left = n1  # Establim fills (left=menor, right=major)
        n3.right = n2
        n1.parent = n3  # Establim pare
        n2.parent = n3

        nodes.append(n3)  # Inserim el nou node
    
    arrel = nodes.pop()
    if verbose:
        print arrel
    
    return arrel


def comprimeix(fitxer_original):
    """
    Funció per comprimir amb Huffman
    """
    fitxer_codificat = fitxer_original + ".cod"
    fitxer_freq = fitxer_original + ".fre"
    fitxer_comprimit = fitxer_original + ".cp2"

    mida_original = os.stat(fitxer_original).st_size
    print "Comprimint", fitxer_original, "(", mida_original, "bytes ) ...",

    t0 = time.time()

    # Obtenim freqüencies
    try:
        f = open(fitxer_original, mode='rb')  # Obrim fitxer a comprimir en mode utf-8
        diccionari = {}  # Hash {caracter, node}

        c = f.read(1)  # Llegim el primer byte del fitxer.
        while c:
            if c in diccionari:  # Si existeix la clau, incrementem comptador al node
                diccionari[c].freq += 1
            else:
                n = Node()  # Altre cas, inserim un nou node amb el caracter com a clau
                n.data = c
                diccionari[c] = n
            c = f.read(1)
        f.close()
    except IOError:
        print "IOError: Error al llegir el fitxer " + fitxer_original + \
              ". Comprova que el fitxer existeix i que tens drets de lectura."
        exit(1)

    # Escribim el fitxer de frequencies
    try:
        w = open(fitxer_freq, mode='wb')
        for i in diccionari.itervalues():
            w.write(i.data + str(i.freq) + '\n')
        w.close()
    except IOError:
        print "IOERROR: No es pot escriure el fitxer " + fitxer_freq + \
              ". Comprova que tens drets d'escriptura al directori."
        exit(1)

    # Construim l'arbre
    arrel = crea_arbre(diccionari.itervalues())

    # Obtenim la codificació de cada caràcter
    establir_codis(arrel)

    # Obtenim el rendiment del codi
    rendiment, entropia, eficiencia = calcula_stats(diccionari.itervalues(), mida_original)

    try:
        f = open(fitxer_original, mode='rb')  # Obrim fitxer a comprimir en mode utf-8

        codificat = str()
        c = f.read(1)  # Llegim el fitxer byte a byte.
        while c:
            codificat += diccionari[c].code
            c = f.read(1)

        f.close()
    except IOError:
        print "IOError: Error al llegir el fitxer " + fitxer_original + \
              ". Comprova que el fitxer existeix i que tens drets de lectura."
        exit(1)

    zeros_extra = 8 - (len(codificat) % 8)  # guardem nombre de zeros
    codificat += '0' * zeros_extra
    
    if verbose:
        print zeros_extra, "zeros afegits al final."

    # Escribim el fitxer codificat en binari
    try:
        w = open(fitxer_codificat, mode='wb')
        w.write(codificat)
        w.close()
    except IOError:
        print "IOERROR: No es pot escriure el fitxer " + fitxer_codificat + \
              ". Comprova que tens drets d'escriptura al directori."
        exit(1)

    # Obtenir text codificat en utf-8 i afegir el número de zeros afegits al principi i escriure al fitxer
    try:
        w = open(fitxer_comprimit, mode='wb')
        w.write(unichr(zeros_extra))  # escribim el nombre de zeros afegits al final de la codificació

        i = 0
        j = 8
        chunk = codificat[i:j]  # prenem chunks de 8 caràcters, que representen els 8 bits d'un sol caràcter
        while chunk:
            val = int(chunk, 2)  # Passem el chunk a enter
            w.write('%c' % val)  # Prenem el caràcter unicode corresponent valor i escribim al fitxer
            i += 8  # incrementem índexos
            j += 8
            chunk = codificat[i:j]  # prenem el següent chunk
        w.close()
    except IOError:
        print "IOERROR: No es pot escriure el fitxer " + fitxer_comprimit + \
              ". Comprova que tens drets d'escriptura al directori."
        exit(1)

    t1 = time.time()
    temps_compressio = t1 - t0
    mida_codificat = os.stat(fitxer_codificat).st_size
    mida_comprimit = os.stat(fitxer_comprimit).st_size
    compress_ratio = float(mida_comprimit) / mida_original

    print "OK"
    if not verbose:
        print "Temps de compressió: ", temps_compressio, "segons."
    print "L'arxiu s'ha comprimit com a", fitxer_comprimit, "(", mida_comprimit, "bytes )"
    print "Compress ratio:", compress_ratio
    print "També s'ha desat el fitxer codificat com a", fitxer_codificat, "(", mida_codificat, "bytes )"
    print "ESTADÍSTIQUES DE COMPRESSIÓ:\n\t* Rendiment:", rendiment, "\n\t* Entropía:",\
        entropia, "\n\t* Eficiència:", eficiencia
    print "La compressió s'ha realitzat amb èxit!"


def descomprimeix(fitxer_comprimit):
    """
    Funció per descomprimir amb Huffman
    """
    fitxer_original = '.'.join(fitxer_comprimit.split('.')[:-1])  # Treiem el .cp2 del fitxer comprimit
    fitxer_freq = fitxer_original + ".fre"
    fitxer_decodificat = fitxer_original + ".decoded"

    mida_comprimit = os.stat(fitxer_comprimit).st_size
    print "Descomprimint", fitxer_comprimit, "(", mida_comprimit, "bytes ) ...",

    t0 = time.time()

    # Llegim el fitxer de freqüencies i reconstruïm l'arbre
    nodes = []  # Llista on afegirem les fulles de l'arbre (nodes amb caràcter i freqüència)

    try:
        f = open(fitxer_freq, mode='rb')
        c = f.read(1)  # Llegim el primer byte (caràcter més freqüent)
        while c:
            freq = str()  # Buffer per la freqüencia
            x = f.read(1)
            while x != "\n":  # Concatenem caràcters fins trobar un salt de línia
                freq += x
                x = f.read(1)

            n = Node()  # Creem un nou Node i establim el caràcter i freqüència
            n.data = c
            n.freq = int(freq)
            nodes.append(n)  # Afegim el nou Node a la llista de nodes

            c = f.read(1)  # llegim el següent caràcter
        f.close()
    except IOError:
        print "IOError: Error al llegir el fitxer " + fitxer_freq + \
              ". Comprova que el fitxer existeix i que tens drets de lectura."
        exit(1)

    # Construïm l'arbre
    arrel = crea_arbre(nodes)  # obtenim l'arrel de l'arbre per decodificar

    try:
        # Llegim el fitxer comprimit i extreiem el text codificat en binari
        f = open(fitxer_comprimit, mode='rb')
        # obtenim el primer caràcter que indica el nombre de zeros extra al final del fitxer en binari
        zeros_extra = ord(f.read(1))
        comp = f.read()  # Obtenim la resta del text comprimit
        f.close()
    except IOError:
        print "IOError: Error al llegir el fitxer " + fitxer_comprimit + \
              ". Comprova que el fitxer existeix i que tens drets de lectura."
        exit(1)

    binari = str()  # String per emmagatzemar el codi binari del fitxer comprimit
    for c in comp:
        b = format((ord(c)), '08b')  # formatem el byte perque tingui 8 caràcters omplint amb zeros a l'esquerra si cal
        binari += b  # concatenem el codi binari del byte

    binari = binari[0:-1*zeros_extra]  # eliminem els zeros extra del final
    
    # Recorrem el text codificat alhora que l'arbre i quan arribem a una fulla, decodifiquem el caràcter
    decodificat = str()  # Buffer pel text decodificat
    curr = arrel
    if verbose:
        print "Decodificant:"
    for i in binari:
        if verbose:
            print i,
        if i == '0':  # Baixem pels fills segons els valors binaris (0=fill_esquerre, 1=fill_dret)
            curr = curr.left
        else:  # if i == '1'
            curr = curr.right
        
        if not curr.left:  # Si NO té fill, és un node intermig. Concatenem el caràcter al text i tornem a l'arrel
            decodificat += curr.data
            if verbose:
                print "->", curr.data
            curr = arrel

    try:
        f = open(fitxer_decodificat, mode="wb")
        f.write(decodificat)
        f.close()
    except IOError:
        print "IOERROR: No es pot escriure el fitxer " + fitxer_decodificat + \
              ". Comprova que tens drets d'escriptura al directori."
        exit(1)

    t1 = time.time()
    temps_descompressio = t1 - t0

    mida_decodificat = os.stat(fitxer_decodificat).st_size
    compress_ratio = float(mida_comprimit) / mida_decodificat

    print "OK"
    if not verbose:
        print "Temps de descompressió: ", temps_descompressio, "segons."
    print "L'arxiu s'ha descomprimit com a", fitxer_decodificat, "(", mida_decodificat, "bytes )"
    print "Compress ratio:", compress_ratio
    print "La descompressió s'ha realitzat amb èxit!"


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Compressor / descompressor Huffman v1.0")
    parser.add_argument("action", choices=["comprimir", "descomprimir"])
    parser.add_argument("file", type=str, help="fitxer font", )
    parser.add_argument("-v", help="mostra informació detallada durant el procés", action="store_true")
    args = parser.parse_args()

    verbose = args.v

    if args.action == "comprimir":
        comprimeix(args.file)
    else:
        descomprimeix(args.file)
