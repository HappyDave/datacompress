#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
import argparse
import os

"""
lempel_ziv.py - David Santos, Oscar Fernandez 2014
"""


def crear_dic(elements, key):
    """
    Creació taula hash amb la representació dels "elements" primers caràcters del UTF-8 en Hex
    """
    diccionari = {}  # Hash <key: cadena en hex, value: numeració>
    if key == "cadena":
        for i in range(elements):
            diccionari[format(i, '02x')] = i
        return diccionari
    elif key == "numeracio":
        for i in range(elements):
            diccionari[i] = format(i, '02x')
        return diccionari
    else:
        raise Exception("Debug: key ha de ser 'cadena' o 'numeració'!")


def comprimeix(fitxer_original):
    """
    Funció per comprimir amb Lempel-Ziv
    """
    fitxer_comprimit = fitxer_original + ".cp2"
    data_compress = []  # Llista on anirà la codificació
    elements = 256  # (256 => UTF-8)

    mida_original = os.stat(fitxer_original).st_size
    print "Comprimint", fitxer_original, "(", mida_original, "bytes ) ...",

    t0 = time.time()

    # Crear dic amb els Nº d'elements inicials del format Unicode que contindrà la hash (256 => UTF-8)
    diccionari = crear_dic(elements, "cadena")

    try:
        f = open(fitxer_original, mode='rb')  # Obrim fitxer a comprimir en mode binary
        data = f.read()
        f.close()
    except IOError:
        print "IOError: Error al llegir el fitxer " + fitxer_original + \
              ". Comprova que el fitxer existeix i que tens drets de lectura."
        exit(1)

    # Creació llista amb la codificació corresponent al fitxer
    i = 0
    increment = 1
    while i < len(data):
        c = data[i].encode("hex")
        if i + 1 < len(data):  # Si no és l'últim caràcter de l'string, l'unim amb el següent caràcter
            j = i + 1
            # Mentres "j" no és l'últim caràcter de l'string i la cadena "c" existeixi en la hash
            while j < len(data) and c in diccionari:
                c += data[j].encode("hex")
                j += 1
            if elements < 2**16:  # Num entrades maximes que permeses en el diccionari
                diccionari[c] = elements  # Afegir la nova cadena en la hash
                if verbose:
                    print "key:", c, "value:", elements
                elements += 1
            c = c[:-2]  # Eliminar darrers caràcters de la cadena hexadecimal
            increment = len(c)/2
        data_compress.append(diccionari[c])  # Guardar en una llista el num que apunta a la cadena
        i += increment

    # Guardar llista amb la codificació en un fitxer
    try:
        f = open(fitxer_comprimit, mode="wb")
        for item in data_compress:
            binary = format(item, '016b')
            bin_tupla = (binary[:8], binary[8:])
            val = '%c' % int(bin_tupla[0], 2)
            val += '%c' % int(bin_tupla[1], 2)

            f.write(val)
        f.close()
    except IOError:
        print "IOERROR: No es pot escriure el fitxer " + fitxer_comprimit + \
              ". Comprova que tens drets d'escriptura al directori."
        exit(1)

    t1 = time.time()
    temps_compressio = t1 - t0
    mida_comprimit = os.stat(fitxer_comprimit).st_size
    compress_ratio = float(mida_comprimit) / mida_original

    print "OK"
    if not verbose:
        print "Temps de compressió: ", temps_compressio, "segons."
    print "L'arxiu s'ha comprimit com a", fitxer_comprimit, "(", mida_comprimit, "bytes )"
    print "Compress ratio:", compress_ratio
    print "La compressió s'ha realitzat amb èxit!"


def descomprimeix(fitxer_comprimit):
    """
    Funció per descomprimir amb Lempel-Ziv
    """
    fitxer_original = '.'.join(fitxer_comprimit.split('.')[:-1])  # Treiem el .cp2 del fitxer comprimit
    fitxer_decodificat = fitxer_original + ".decoded"
    elements = 256  # (256 => UTF-8)

    mida_comprimit = os.stat(fitxer_comprimit).st_size
    print "Descomprimint", fitxer_comprimit, "(", mida_comprimit, "bytes ) ...",

    t0 = time.time()

    # Crear dic amb els Nº d'elements inicials del format Unicode que contindrà la hash (256 => UTF-8)
    diccionari = crear_dic(elements, "numeracio")

    try:
        f = open(fitxer_comprimit, mode='rb')  # Obrim fitxer a comprimir en mode binary
        data = f.read()
        f.close()
    except IOError:
        print "IOError: Error al llegir el fitxer " + fitxer_comprimit + \
              ". Comprova que el fitxer existeix i que tens drets de lectura."
        exit(1)

    i = 0
    bin_tupla_actual = (data[i], data[i+1])
    b_actual = format((ord(bin_tupla_actual[0])), '08b') + format((ord(bin_tupla_actual[1])), '08b')
    key_actual = int(b_actual, 2)
    val_actual = diccionari[key_actual].decode('hex')
    
    data_uncompress = val_actual
    
    i += 2

    while i < len(data):
        if verbose:
            print "val_actual:", val_actual
        
        bin_tupla_seg = (data[i], data[i+1])
        b_seg = format((ord(bin_tupla_seg[0])), '08b') + format((ord(bin_tupla_seg[1])), '08b')
        key_seg = int(b_seg, 2)

        if key_seg in diccionari:
            val_seg = diccionari[key_seg].decode('hex')
        elif key_seg == len(diccionari):
            val_seg = val_actual + val_actual[0]
        else:
            print("Error de compressio. Index %d" % val_seg)
            exit(1)

        if verbose:
            print "val_seg:", val_seg

        data_uncompress += val_seg
        
        # en principi no cal limitar el diccionari al descomprimir, ja que al reconstruir el diccionari, aquest no
        # hauria de ser mai diferent de l'original.
        #if elements < 2**16:            # Num entrades maximes que permeses en el diccionari
        nou_valor = val_actual + val_seg[0]
        diccionari[elements] = nou_valor.encode('hex')
        if verbose:        
            print "key:", elements, "value:", nou_valor.encode('hex')
        elements += 1

        val_actual = val_seg

        i += 2

    # Guardar llista amb la decodificació en un fitxer
    try:
        f = open(fitxer_decodificat, mode="wb")
        f.write(data_uncompress)
        f.close()
    except IOError:
        print "IOERROR: No es pot escriure el fitxer " + fitxer_decodificat + \
              ". Comprova que tens drets d'escriptura al directori."
        exit(1)

    t1 = time.time()
    temps_descompressio = t1 - t0

    mida_decodificat = os.stat(fitxer_decodificat).st_size
    compress_ratio = float(mida_comprimit) / mida_decodificat

    print "OK"
    if not verbose:
        print "Temps de descompressió: ", temps_descompressio, "segons."
    print "L'arxiu s'ha descomprimit com a", fitxer_decodificat, "(", mida_decodificat, "bytes )"
    print "Compress ratio:", compress_ratio
    print "La descompressió s'ha realitzat amb èxit!"


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Compressor / descompressor Lempel-Ziv-Welch v1.0")
    parser.add_argument("action", choices=["comprimir", "descomprimir"])
    parser.add_argument("file", type=str, help="fitxer font", )
    parser.add_argument("-v", help="mostra informació detallada durant el procés", action="store_true")
    args = parser.parse_args()

    verbose = args.v

    if args.action == "comprimir":
        comprimeix(args.file)
    else:
        descomprimeix(args.file)